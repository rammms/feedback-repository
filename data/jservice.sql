--
-- PostgreSQL test database
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

-- connect to database
-- named in docker-compose.yml
\connect saunter

-- create table for states
-- CREATE TABLE IF NOT EXISTS saunter_rest_state (
--     id serial PRIMARY KEY,
--     state_name varchar (100) NOT NULL,
--     abbrev varchar (2) NOT NULL
-- );

-- INSERT INTO saunter_rest_state (state_name, abbrev)
-- VALUES
--     ('AK', 'Alaska'),
--     ('AL', 'Alabama'),
--     ('AZ', 'Arizona'),
--     ('AR', 'Arkansas'),
--     ('CA', 'California'),
--     ('CO', 'Colorado'),
--     ('CT', 'Connecticut'),
--     ('DE', 'Delaware'),
--     ('DC', 'District of Columbia'),
--     ('FL', 'Florida'),
--     ('GA', 'Georgia'),
--     ('HI', 'Hawaii'),
--     ('ID', 'Idaho'),
--     ('IL', 'Illinois'),
--     ('IN', 'Indiana'),
--     ('IA', 'Iowa'),
--     ('KS', 'Kansas'),
--     ('KY', 'Kentucky'),
--     ('LA', 'Louisiana'),
--     ('ME', 'Maine'),
--     ('MD', 'Maryland'),
--     ('MA', 'Massachusetts'),
--     ('MI', 'Michigan'),
--     ('MN', 'Minnesota'),
--     ('MS', 'Mississippi'),
--     ('MO', 'Missouri'),
--     ('MT', 'Montana'),
--     ('NE', 'Nebraska'),
--     ('NV', 'Nevada'),
--     ('NH', 'New Hampshire'),
--     ('NJ', 'New Jersey'),
--     ('NM', 'New Mexico'),
--     ('NY', 'New York'),
--     ('NC', 'North Carolina'),
--     ('ND', 'North Dakota'),
--     ('OH', 'Ohio'),
--     ('OK', 'Oklahoma'),
--     ('OR', 'Oregon'),
--     ('PA', 'Pennsylvania'),
--     ('PR', 'Puerto Rico'),
--     ('RI', 'Rhode Island'),
--     ('SC', 'South Carolina'),
--     ('SD', 'South Dakota'),
--     ('TN', 'Tennessee'),
--     ('TX', 'Texas'),
--     ('UT', 'Utah'),
--     ('VT', 'Vermont'),
--     ('VA', 'Virginia'),
--     ('WA', 'Washington'),
--     ('WV', 'West Virginia'),
--     ('WI', 'Wisconsin'),
--     ('WY', 'Wyoming');