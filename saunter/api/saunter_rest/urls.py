from django.urls import path


from .views import (
    api_detail_review,
    api_detail_trip,
    api_list_locations,
    api_list_reviews,
    api_list_states,
    api_list_trips,
    api_show_location,
    api_users,
    api_user_token,
    api_list_ratings,
    api_detail_rating,
)

urlpatterns = [
    path("users/", api_users, name="api_users"),
    path("users/me/token/", api_user_token, name="api_user_token"),
    path("locations/", api_list_locations, name="api_locations"),
    path("locations/<int:pk>", api_show_location, name="api_show_location"),
    path("states/", api_list_states, name="api_states"),
    path("trips/", api_list_trips, name="api_trips"),
    path("trips/<int:pk>", api_detail_trip, name="api_detail_trip"),
    path("reviews/", api_list_reviews, name="api_reviews"),
    path("reviews/<int:pk>/", api_detail_review, name="api_detail_review"),
    path("ratings/", api_list_ratings, name="api_ratings"),
    path("ratings/<int:pk>/", api_detail_rating, name="api_detail_rating"),
]
