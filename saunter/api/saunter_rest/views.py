from django.db import IntegrityError
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import User, Location, State, Trip, Review, Rating
from common.json import ModelEncoder
from .acls import get_weather_data


@require_http_methods(["GET", "POST"])
def api_users(request):
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            User.objects.create_user(**content)
            return JsonResponse(
                {
                    # We commented this out b/c it was redering after
                    # signing up and we also don't have email as part
                    # of the sign up form
                    # "username": user.username,
                    # "email": user.email,
                }
            )
        except IntegrityError:
            response = JsonResponse({"detail": "bad credentials"})
            response.status_code = 400
            return response


@require_http_methods(["GET"])
def api_user_token(request):
    if "jwt_access_token" in request.COOKIES:
        token = request.COOKIES["jwt_access_token"]
        if token:
            return JsonResponse(
                {
                    "token": token,
                },
            )
    response = JsonResponse({"detail": "no session"})
    response.status_code = 404
    return response


class StateDetailEncoder(ModelEncoder):
    model = State
    properties = ["id", "name", "abbreviation"]


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name", "city", "state", "id"]
    encoders = {
        "state": StateDetailEncoder(),
    }

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = ["name", "city", "state", "id"]
    encoders = {
        "state": StateDetailEncoder(),
    }

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class TripListEncoder(ModelEncoder):
    model = Trip
    properties = ["title", "date", "total_cost", "location", "id", "user"]
    encoders = {
        "location": LocationListEncoder(),
    }

    def get_extra_data(self, o):
        return {"user": o.user.username}


class TripDetailEncoder(ModelEncoder):
    model = Trip
    properties = ["title", "date", "total_cost", "user", "id"]
    encoders = {
        "location": LocationListEncoder(),
    }

    def get_extra_data(self, o):
        return {"user": o.user.username}


class ReviewListEncoder(ModelEncoder):
    model = Review
    properties = [
        "id",
        "author",
        "trip",
        "description",
        "lodging",
        "pictures",
        "rec_restaurants",
    ]
    encoders = {
        "trip": TripListEncoder(),
    }

    def get_extra_data(self, o):

        return {"author": o.author}


class ReviewDetailEncoder(ModelEncoder):
    model = Review
    properties = [
        "id",
        "author",
        "description",
        "content",
        "lodging",
        "rec_restaurants",
    ]
    encoders = {
        "trip": TripListEncoder(),
    }

    def get_extra_data(self, o):
        return {"author": o.author}


class RatingListEncoder(ModelEncoder):
    model = Rating
    properties = ["id", "value"]


class RatingDetailEncoder(ModelEncoder):
    model = Rating
    properties = ["id", "value"]
    encoders = {
        "review": ReviewListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:

        content = json.loads(request.body)

        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state

        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        location = Location.objects.create(**content)

        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, pk):
    if request.method == "GET":
        location = Location.objects.get(id=pk)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        Location.objects.filter(id=pk).update(**content)

        location = Location.objects.get(id=pk)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def api_list_states(request):

    states = State.objects.order_by("name")

    state_list = []

    for state in states:
        all_state = {"name": state.name, "abbreviation": state.abbreviation}
        state_list.append(all_state)

    return JsonResponse({"states": state_list})


@require_http_methods(["GET", "POST"])
def api_list_trips(request):
    if request.method == "GET":
        trips = Trip.objects.all()
        return JsonResponse(
            {"trips": trips},
            encoder=TripListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Location object and put it in the content dict
        try:
            location = Location.objects.get(name=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status=400,
            )

        try:
            content["user"] = request.user
        except User.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid username"},
                status=400,
            )

        trip = Trip.objects.create(**content)
        return JsonResponse(
            trip,
            encoder=TripDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_detail_trip(request, pk):
    if request.method == "GET":
        trip = Trip.objects.get(id=pk)
        weather = get_weather_data(
            trip.location.city,
            trip.location.state.abbreviation,
        )

        return JsonResponse(
            {"trip": trip, "weather": weather},
            encoder=TripDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Trip.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(name=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status=400,
            )

        try:
            if "user" in content:
                user = User.objects.get(username=content["user"])
                content["user"] = user
        except User.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid username"},
                status=400,
            )
        Trip.objects.filter(id=pk).update(**content)
        trip = Trip.objects.get(id=pk)
        return JsonResponse(
            trip,
            encoder=TripDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_reviews(request):
    print("screwdocker")
    if request.method == "GET":
        print("here we are4")
        reviews = Review.objects.all()
        return JsonResponse(
            {"reviews": reviews},
            encoder=ReviewListEncoder,
        )

    else:
        content = json.loads(request.body)

        # Get the Trip object and put it in the content dict
        try:
            trip = Trip.objects.get(title=content["trip"])
            content["trip"] = trip
        except Trip.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Trip"},
                status=400,
            )

        review = Review.objects.create(**content)
        return JsonResponse(
            review,
            encoder=ReviewDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_detail_review(request, pk):
    if request.method == "GET":
        review = Review.objects.get(id=pk)
        return JsonResponse(
            {"review": review},
            encoder=ReviewDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Review.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "trip" in content:
                trip = Trip.objects.get(title=content["trip"])
                content["trip"] = trip
        except Trip.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid trip"},
                status=400,
            )

        Review.objects.filter(id=pk).update(**content)
        review = Review.objects.get(id=pk)
        return JsonResponse(
            review,
            encoder=ReviewDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_ratings(request):
    if request.method == "GET":
        ratings = Rating.objects.all()
        return JsonResponse(
            {"ratings": ratings},
            encoder=RatingListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Location object and put it in the content dict
        try:
            review = Review.objects.get(id=content["review"])
            content["review"] = review
        except Review.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid review"},
                status=400,
            )

        rating = Rating.objects.create(**content)
        return JsonResponse(
            rating,
            encoder=RatingDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_detail_rating(request, pk):
    if request.method == "GET":
        rating = Rating.objects.get(id=pk)
        return JsonResponse(
            {"rating": rating},
            encoder=RatingDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Rating.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "review" in content:
                review = Review.objects.get(id=content["review"])
                content["review"] = review
        except Review.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid review"},
                status=400,
            )

        Rating.objects.filter(id=pk).update(**content)
        rating = Rating.objects.get(id=pk)
        return JsonResponse(
            rating,
            encoder=RatingDetailEncoder,
            safe=False,
        )
