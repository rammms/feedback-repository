from django.contrib.auth.models import AbstractUser
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


class State(models.Model):
    id = models.PositiveIntegerField(primary_key=True)
    name = models.CharField(max_length=20)
    abbreviation = models.CharField(max_length=2)

    def __str__(self):
        return f"{self.abbreviation}"

    class Meta:
        ordering = ("abbreviation",)


class Location(models.Model):
    name = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    state = models.ForeignKey(
        State,
        related_name="location",
        on_delete=models.PROTECT,
    )


class User(AbstractUser):
    avatar = models.ImageField(
        upload_to=None,
        height_field=None,
        width_field=None,
        max_length=100,
        null=True,
    )


class Trip(models.Model):
    title = models.CharField(max_length=200)
    location = models.ForeignKey(
        Location,
        related_name="trip",
        on_delete=models.CASCADE,
    )
    date = models.DateField(null=True)
    total_cost = models.PositiveBigIntegerField()
    user = models.ForeignKey(
        User,
        related_name="trip",
        on_delete=models.CASCADE,
    )


class Review(models.Model):
    author = models.CharField(max_length=20)
    trip = models.ForeignKey(
        Trip,
        related_name="review",
        on_delete=models.CASCADE,
    )
    description = models.TextField(null=True)
    content = models.TextField()
    pictures = models.URLField(null=True)
    lodging = models.CharField(max_length=200, null=True)
    rec_restaurants = models.CharField(max_length=200, null=True)


class Rating(models.Model):
    value = models.PositiveSmallIntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1),
        ]
    )
    review = models.ForeignKey(
        Review,
        related_name="rating",
        on_delete=models.CASCADE,
    )
