from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Rating, Review, Trip, User, State, Location


admin.site.register(Rating)
admin.site.register(Review)
admin.site.register(Trip)
admin.site.register(User, UserAdmin)
admin.site.register(State)
admin.site.register(Location)
