"""saunter_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from saunter_rest.views import (
    api_list_locations,
    api_show_location,
    api_list_states,
    api_list_reviews,
)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("djwto.urls")),
    path("api/", include("saunter_rest.urls")),
    path("locations/", api_list_locations, name="api_list_locations"),
    path("locations/<int:pk>/", api_show_location, name="api_show_location"),
    path("states/", api_list_states, name="api_list_states"),
    path("reviews/", api_list_reviews, name="api_list_reviews"),
]
