# Integrations

Of note this project seems very similar to KAYAK so in theory we could just steal everything from there. 

A less prone to lawsuit option would just be pulling from the USA board of tourism for hotels/events/resteraunts https://www.usa.gov/state-travel-and-tourism
This links to each individual state which then link to city tourism baords. The actual travel data industry seems very incestious with several sites pulling from other sites that pull from other sites that actually pay for the data. If we want to avoid relying on stacked third parties then data should be pulled from vendor and government sites. This means we either have to pull small amounts of data from a great many places or a large amount of data from only a few.

Weather Data: By zip code- via Open Weather

Car Rentals: By Airport - Pulling car availability from Several providers I.E Enterprise, Hertz, Etc. or can be pillaged from travelocity

Hotels: By Zip Code- Collate data from orbitz, priceline, expedia, hotelscombined, and travelocity. Also cities have tourism boards often providing services like https://visitseattle.org/lodging/ which can be listed and pulled from

Event Info: By the nearest cluster of zip codes- Pulled from stubhub and local tourism board

Resteraunts: by nearest 3 zip codes- Opentable collated with tourism board data

Flight Prices: by target and starting- location Shamelessly pillage google flight data



