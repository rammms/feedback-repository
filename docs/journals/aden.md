# 6/23
Redirected user who isn't logged in from createblog form to sign in/log in page 
adjusted UI for sign in page to look like log in
created a link once signed in to create a blog post page.
dogpiled triplist, blogreviewlist

# 6/22 
* meleya and i met with curtis before class to work through questions we had about merge conflicts once models have been changed.
changed models - added display name for user so username won't show
changed the picture property in review model to url from upload

added a useeffect hook to blogcardlist so the blogs would render on the main page in the card box.
create blog only for logged in people

changing the models 
delete blog works in insomnia

# 6/20-21
* worked on front end with meleya.  added css elements to the main page. created blog card list to show on main page
layout of the main page - where blogs are available for users who are not authenticated
functionality of being able to sign up from log in page if you don't have a log in 
after lunch we regrouped and michael shared his screen - attempted to fix the create trip - which would need a list of states with abbreviation in order for the blogpost to be able to pull the trip
2-345 PST in zoom room altogether
then meleya and i regrouped and created the listblog file, fixed blog form.  we're able to list and delete blogs. can't create blog because model needs to be changed.  since we are working on a different branch, we held off, and regrouped with the rest of the team to let them know.
we put in a ticket afterhours to be able to pull main into aden so we could have trip form that will be passed onto our blog create form
updated css on the log in form. looks cutie. 

# 6/6 - 6/8
* was able to get the main page to render.  was having issues with react router dom. was able to get that fixed.  created the nav bar with links routing the right place. no css yet though. will work on that today with meleya.  meleya and i were able to successfully redirect user to correct place.  worked through some issues with curtis.  we were able to render the create blog form. meleya is a star pair programmer. would recommend.

# 6/2
* Mya shared her screen and we created the models for the app.

# 6/1
* Debugged in VS because it wasn't working for some of us - getting errors.  Was able to get that fixed.  localhost:3000 is showing the correct thing.

# 5/31
* Group successfully created the Dockerfile.  Server is now able to run.  
