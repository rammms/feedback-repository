# Data models

## Location

| Name | Type | Unique | Optional |
|-|-|-|-|
| name | string | no | no |
| city | string | no | no |
| state | string| no | no |

The `Location` entity contains data about the location the user has been to. 

## User Account
| Name | Type | Unique | Optional |
|-|-|-|-|
| username | string | yes | no |
| password | string | yes | no |
| email | string | yes | no |
| avatar | image | no | yes |

The `User Account` entity contains data about the account the user can open. 

## Trip 
| Name | Type | Unique | Optional |
|-|-|-|-|
| title | string | no | no |
| date  | date | no | yes |
| total cost | integer | no | no |
| location | references to Location entity | no | no |
| user | references to User entity | yes | no |

The `Trip` entity contains data about where the user has traveled to, including the total cost of the trip. 

## Trip Review  
| Name | Type | Unique | Optional |
|-|-|-|-|
| trip | references to the Trip entity | no | no |
| content | text | no | no |
| pictures | image | no | yes |
| rating | integer | no | no |
| user | references to User entity | yes | no |
| lodging | string | no | yes |
| recommended restaurants | string | no | yes |

The `Trip Review` entity contains the data about the users review and recommendation based on the trip they have taken perviously. 
