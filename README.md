# saunter-app

Name of the application: saunter.

List of Team members:
* Richard
* Aden
* Meleya
* Mya
* Michael

Our app 'saunter.' provides a travel log service.

Intended Market:
This app would be helpful for anyone who is interested in traveling but might think planning a trip is daunting.  Providing honest feedback about trips, travelers are able to create a trip blog for other travelers interested in those same locations.
    

Functionality:
* Users can create an account with app
* Users are able to create a travel blog
    * with personal reviews of trip (food, attractions, museums, lodging, etc)
    * Users are able to add photos of trip
* Search functionality - as a user looking for a review based on the place
* Open to everyone but only those with an account can create a blog


Stretch goals
* Geotagging location of photos 
* Usage of tags/filter - being able to click a word and finding all instances
* Users can comment, rate, like etc.