import React from "react";
import { Link, Navigate } from "react-router-dom";

class TripDeleteForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      trip: "",
      trips: [],
      deleted: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleTripSelection = this.handleTripSelection.bind(this);
  }
  async componentDidMount() {
    const url = `${process.env.REACT_APP_ACCOUNTS_HOST}/api/trips/`;

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ trips: data.trips });
    }
  }
  async handleSubmit(event) {
    event.preventDefault();

    const tripURL = `${process.env.REACT_APP_ACCOUNTS_HOST}/api/trips/${this.state.trip}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(tripURL, fetchConfig);
    if (response.ok) {
      const newTrip = await response.json();
      console.log(newTrip);

      let remainingTrips = [];
      for (let i = 0; i < this.state.trips.length; i++) {
        let currentTrip = this.state.trips[i];
        if (parseInt(this.state.trip) !== currentTrip.id) {
          remainingTrips.push(currentTrip);
        }
      }

      this.setState({
        trip: "",
        trips: remainingTrips,
        deleted: true,
      });
    }
  }

  handleTripSelection(event) {
    const value = event.target.value;
    this.setState({ trip: value });
  }

  render() {
    if (!this.props.token) {
      return (
        <div className="px-4 py-5 my-5 text-center">
          You do not have access to this page. Please
          <Link to="/login"> login </Link>
          to view this content.
        </div>
      );
    }
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Delete Which Trip?</h1>
            <form onSubmit={this.handleSubmit} id="delete-trip-form">
              <div className="mb-3">
                <select
                  onChange={this.handleTripSelection}
                  value={this.state.trip}
                  required
                  name="trip"
                  id="trip"
                  className="form-select"
                >
                  <option value="">Choose an trip</option>
                  {this.state.trips.map((trip) => {
                    return (
                      <option key={trip.id} value={trip.id}>
                        {trip.title}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Remove</button>
            </form>
          </div>
          {this.state.deleted && <Navigate to="/yourtrips"></Navigate>}
        </div>
      </div>
    );
  }
}

export default TripDeleteForm;
