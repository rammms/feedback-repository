import { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";

function BlogList(props) {
  const token = props.token;
  const [reviews, setReviews] = useState([]);
  const navigate = useNavigate();
  const deleteBlog = () => {
    navigate("/blogdelete");
  };
  useEffect(() => {
    const fetchReviews = async () => {
      const reviewUrl = `${process.env.REACT_APP_ACCOUNTS_HOST}/api/reviews/`;
      const response = await fetch(reviewUrl);
      if (response.ok) {
        const reviewData = await response.json();
        setReviews(reviewData.reviews);
        console.log(reviewData.reviews);
      }
    };
    fetchReviews();
  }, []);
  if (!token) {
    return (
      <div className="px-4 py-5 my-5 text-center">
        You do not have access to this page, please
        <Link to="/login"> login </Link>
        to view this content.
      </div>
    );
  }
  return (
    <div className="container">
      <h2 className="display-5 fw-bold">List of Posts</h2>
      <div className="row">
        {reviews.map((review) => {
          return (
            <div className="container">
              <div key={review.id} className="col">
                <div className="card mb-3 shadow">
                  <div className="card-body">
                    <img
                      className="card-img-top"
                      src={review.pictures}
                      alt=""
                    />
                    <h5 className="card-title">{review.trip.title}</h5>
                    <h6 className="card-subtitle mb-2 text-muted">
                      {review.author}
                    </h6>
                    <h5 className="card-text">{review.description}</h5>
                    <h5 className="card-text">{review.lodging}</h5>
                    <h5 className="card-text">{review.rec_restaurants}</h5>
                  </div>
                  <footer class="text-center text-lg-start bg-light text-muted">
                    <div className="text-center p-4"></div>
                  </footer>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      <button className="p-2 bg-light border" onClick={deleteBlog}>
        Delete a blog
      </button>
    </div>
  );
}

export default BlogList;
