import { useState } from "react";
import "./App.css";
import { NavLink, Navigate } from "react-router-dom";

function LogIn(props) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);
  const token = props.token;
  const login = props.login;
  const logout = props.logout;

  function log(value) {
    console.log(value);
    return value;
  }

  return (
    <div>
      {error ? <div dangerouslySetInnerHTML={{ __html: error }} /> : null}
      {token ? (
        <div>
          <h1>You're logged in!</h1>
          <div>
            <Navigate to="/homepage"></Navigate>
          </div>
          <button className="btn btn-danger" onClick={logout}>
            Logout
          </button>
        </div>
      ) : (
        <div>
          <div className="container">
            <h1>Login</h1>
            <form className="px-4 py-3" onSubmit={(e) => e.preventDefault()}>
              <div className="form-group">
                <label htmlFor="username">Enter Username</label>
                <input
                  className="form-control"
                  required
                  type="text"
                  onChange={(e) => setUsername(e.target.value)}
                  value={username}
                  placeholder="username"
                />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input
                  className="form-control"
                  required
                  type="password"
                  onChange={(e) => setPassword(e.target.value)}
                  value={password}
                  placeholder="password"
                />
              </div>

              <button
                className="btn btn-danger"
                onClick={async () =>
                  setError(log(await login(username, password)))
                }
              >
                Login
              </button>
            </form>
            <div>
              <NavLink className="nav-link" to="/signup">
                {" "}
                New around here? Sign up{" "}
              </NavLink>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default LogIn;
