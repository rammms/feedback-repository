import React from "react";
import { Link, Navigate, NavLink } from "react-router-dom";

class TripCreateForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      locations: [],
      date: "",
      total_cost: "",
      created: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeTitle = this.handleChangeTitle.bind(this);
    this.handleChangeLocation = this.handleChangeLocation.bind(this);
    this.handleChangeDate = this.handleChangeDate.bind(this);
    this.handleChangeTotal_Cost = this.handleChangeTotal_Cost.bind(this);
  }
  async componentDidMount() {
    const url = `${process.env.REACT_APP_ACCOUNTS_HOST}/locations/`;

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ locations: data.locations });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.locations;
    delete data.created;
    console.log(data);

    const locationUrl = `${process.env.REACT_APP_ACCOUNTS_HOST}/api/trips/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    console.log("sumbit", response);
    if (response.ok) {
      const currentlocation = await response.json();
      console.log(currentlocation);
      this.setState({
        name: "",
        city: "",
        date: "",
        state: [],
        created: true,
      });
    }
  }

  handleChangeTitle(event) {
    const value = event.target.value;
    this.setState({ title: value });
  }
  handleChangeDate(event) {
    const value = event.target.value;
    this.setState({ date: value });
  }
  handleChangeTotal_Cost(event) {
    const value = event.target.value;
    this.setState({ total_cost: value });
  }
  handleChangeLocation(event) {
    const value = event.target.value;
    this.setState({ location: value });
  }
  render() {
    if (!this.props.token) {
      return (
        <div className="px-4 py-5 my-5 text-center">
          You do not have access to this page, please
          <Link to="/login"> login </Link>
          to view this content.
        </div>
      );
    }
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>How Was Your Trip? {this.state.user}</h1>
            <form onSubmit={this.handleSubmit} id="create-model-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeTitle}
                  value={this.state.title}
                  placeholder="Title"
                  required
                  type="text"
                  name="title"
                  id="title"
                  className="form-control"
                />
                <label htmlFor="title">Trip Name:</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeDate}
                  value={this.state.date}
                  placeholder="Date"
                  required
                  type="date"
                  name="date"
                  id="date"
                  className="form-control"
                />
                <label htmlFor="date">Date</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeTotal_Cost}
                  value={this.state.total_cost}
                  placeholder="Total_Cost"
                  required
                  type="number"
                  name="total_cost"
                  id="total_cost"
                  className="form-control"
                />
                <label htmlFor="total_cost">Cost Of Your Trip:</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleChangeLocation}
                  value={this.state.location}
                  required
                  name="location"
                  id="location"
                  className="form-select"
                >
                  <option value="">Choose a location</option>
                  {this.state.locations.map((location) => {
                    console.log(location);
                    return (
                      <option key={location.id} value={location.name}>
                        {location.name} in {location.city} {location.state.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            {this.state.created && <Navigate to="/yourtrips"></Navigate>}
            <div>
              <NavLink className="nav-link" to="/newlocation">
                {" "}
                Add a new location? Click here.{" "}
              </NavLink>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TripCreateForm;
