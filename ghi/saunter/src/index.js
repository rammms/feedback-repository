import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";

const root = ReactDOM.createRoot(document.getElementById("root"));
async function loadDatabase() {
  let tripData, locationData, stateData, reviewData;
  // Use process.env.REACT_APP_ACCOUNTS_HOST to use the
  // proper URL DOMAIN to get stuff. MAKE SURE TO BACKTICK
  // YOUR STRING.
  const tripResponse = await fetch(
    `${process.env.REACT_APP_ACCOUNTS_HOST}/api/trips/`
  );
  const locationResponse = await fetch(
    `${process.env.REACT_APP_ACCOUNTS_HOST}/api/locations/`
  );
  const stateResponse = await fetch(
    `${process.env.REACT_APP_ACCOUNTS_HOST}/api/states/`
  );
  const reviewResponse = await fetch(
    `${process.env.REACT_APP_ACCOUNTS_HOST}/api/reviews/`
  );

  if (locationResponse.ok) {
    locationData = await locationResponse.json();
    console.log("location data: ", locationData);
  } else {
    console.error(locationResponse);
  }
  if (tripResponse.ok) {
    tripData = await tripResponse.json();
    console.log("trip data: ", tripData);
  } else {
    console.error(tripResponse);
  }
  if (stateResponse.ok) {
    stateData = await stateResponse.json();
    console.log("state data: ", stateData);
  } else {
    console.error(stateResponse);
  }
  if (reviewResponse.ok) {
    reviewData = await reviewResponse.json();
    console.log("review data: ", reviewData);
  } else {
    console.error(reviewResponse);
  }

  root.render(
    <React.StrictMode>
      <App
        trips={tripData.trips}
        locations={locationData.locations}
        states={stateData.states}
        reviews={reviewData.reviews}
      />
    </React.StrictMode>
  );
}
loadDatabase();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
