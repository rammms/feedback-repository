import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

function BlogCardList(props) {
  const [/* reviews */, setReviews] = useState([
    {
      trip: { title: "" },
    },
  ]);

  let navigate = useNavigate();

  const handleClick = function (e) {
    const value = e.target.value;
    props.setReviewId(value);
    navigate("../details", { replace: true });
  };

  useEffect(() => {
    const fetchBlogs = async () => {
      const url = `${process.env.REACT_APP_ACCOUNTS_HOST}/api/reviews/`;
      const response = await fetch(url);
      if (response.ok) {
        const blogData = await response.json();
        setReviews(blogData.reviews);
        console.log("****", blogData.reviews);
      }
    };
    fetchBlogs();
  }, []);

  return (
    <div className="card-group">
      {props.filterData.map((review) => {
        console.log("trip", review);
        return (
          <div className="card">
            <img
              className="card-img-top"
              src={review.pictures}
              alt=""
            />
            <div className="card-body">
              <h5 className="card-title">{review.trip.title}</h5>

              <p className="card-text">{review.description}</p>
            </div>
            <div>
              <button
                className="btn btn-danger"
                value={review.id}
                onClick={handleClick}
              >
                Read Me
              </button>
            </div>
            <div className="card-footer">
              <small className="text-muted">Updated 3 mins ago</small>
            </div>
          </div>
        );
      })}
    </div>
  );
}

export default BlogCardList;
