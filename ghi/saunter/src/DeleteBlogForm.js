import React from "react";
import { Link, Navigate } from "react-router-dom";

class DeleteBlogForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      review: "",
      reviews: [],
      deleted: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleReviewsSelection = this.handleReviewsSelection.bind(this);
  }
  async componentDidMount() {
    const url = `${process.env.REACT_APP_ACCOUNTS_HOST}/api/reviews/`;

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ reviews: data.reviews });
    }
  }
  async handleSubmit(event) {
    event.preventDefault();
    const reviewURL = `${process.env.REACT_APP_ACCOUNTS_HOST}/api/reviews/${this.state.review}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(reviewURL, fetchConfig);
    if (response.ok) {
      const newReview = await response.json();
      console.log(newReview);

      let remainingReviews = [];
      for (let i = 0; i < this.state.reviews.length; i++) {
        let currentReview = this.state.reviews[i];
        if (parseInt(this.state.review) !== currentReview.id) {
          remainingReviews.push(currentReview);
        }
      }

      this.setState({
        review: "",
        reviews: remainingReviews,
        deleted: true,
      });
    }
  }

  handleReviewsSelection(event) {
    const value = event.target.value;
    this.setState({ review: value });
  }

  render() {
    if (!this.props.token) {
      return (
        <div className="px-4 py-5 my-5 text-center">
          You do not have access to this page. Please
          <Link to="/login"> login </Link>
          to view this content.
        </div>
      );
    }
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Which review would you like to delete?</h1>
            <form onSubmit={this.handleSubmit} id="delete-blog-form">
              <div className="mb-3">
                <select
                  onChange={this.handleReviewsSelection}
                  value={this.state.review}
                  required
                  name="review"
                  id="review"
                  className="form-select"
                >
                  <option value="">Choose a Review</option>
                  {this.state.reviews.map((review) => {
                    return (
                      <option key={review.id} value={review.id}>
                        {review.trip.title}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Delete</button>
            </form>
          </div>
          {this.state.deleted && <Navigate to="/reviews"></Navigate>}
        </div>
      </div>
    );
  }
}

export default DeleteBlogForm;
