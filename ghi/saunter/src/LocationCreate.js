import React from "react";
import { Link, Navigate } from "react-router-dom";

class LocationCreateForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      states: [],
      city: "",
      created: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeState = this.handleChangeState.bind(this);
    this.handleChangeCity = this.handleChangeCity.bind(this);
  }
  async componentDidMount() {
    const url = `${process.env.REACT_APP_ACCOUNTS_HOST}/states/`;

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ states: data.states });
    }
  }
  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.states;
    delete data.created;
    console.log(data);

    const stateUrl = `${process.env.REACT_APP_ACCOUNTS_HOST}/api/locations/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(stateUrl, fetchConfig);
    console.log("sumbit", response);
    if (response.ok) {
      const currentstate = await response.json();
      console.log("testinghere", currentstate);
      this.setState({
        id: "",
        name: "",
        abbreviation: "",
        created: true,
      });
    }
  }

  handleChangeName(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }
  handleChangeCity(event) {
    const value = event.target.value;
    this.setState({ city: value });
  }
  handleChangeState(event) {
    const value = event.target.value;
    this.setState({ state: value });
  }
  render() {
    if (!this.props.token) {
      return (
        <div className="px-4 py-5 my-5 text-center">
          You do not have access to this page, please
          <Link to="/login"> login </Link>
          to view this content.
        </div>
      );
    }
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Where did you go? {this.state.user}</h1>
            <form onSubmit={this.handleSubmit} id="create-model-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeName}
                  value={this.state.name}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="title">Location Name:</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeCity}
                  value={this.state.city}
                  placeholder="City"
                  required
                  type="text"
                  name="city"
                  id="city"
                  className="form-control"
                />
                <label htmlFor="total_cost">City:</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleChangeState}
                  value={this.state.state}
                  required
                  name="state"
                  id="state"
                  className="form-select"
                >
                  <option value="">Choose a state</option>
                  {this.state.states.map((state) => {
                    console.log("logging", state);
                    return (
                      <option key={state.id} value={state.abbreviation}>
                        {state.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            {this.state.created && <Navigate to="/newtrip"></Navigate>}
          </div>
        </div>
      </div>
    );
  }
}

export default LocationCreateForm;
