import { useState } from "react";
import "./App.css";
import { useEffect } from "react";

import BlogCardList from "./BlogCardList";

function MainPage(props) {
  const [query, setQuery] = useState("");
  // sets our query to blank
  const [data, setData] = useState([]);
  // sets our data to whet is pulled from api
  const [filterData, setFilterData] = useState([]);
  // creates an array for filterdata
  console.log("this is raw ", data);

  // where you get your data from
  useEffect(() => {
    async function getTrip() {
      const fetchConfig = {
        method: "get",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
      };
      const tripApiUrl = `${process.env.REACT_APP_ACCOUNTS_HOST}/api/reviews/`;

      const response = await fetch(tripApiUrl, fetchConfig);
      if (response.ok) {
        const currentTrip = await response.json();
        console.log(currentTrip);
        setData(currentTrip.reviews);
      }
    }
    getTrip();
  }, []);
  useEffect(() => {
    // this allows this segment of code to be called multiple times
    const filterData = data.filter((reviews) => {
      //defines what filterData is
      if (query === "") {
        return reviews;
        // what it returns if search bar is blank
      } else if (
        reviews.trip.location.city.toLowerCase().includes(query.toLowerCase())
      ) {
        // to change what you are searching by alter the destination before .toLowercase
        return reviews;
        // What it returns based on query input and lso checks for lowercase
      }
      return false;
    });
    if (filterData.length > 5) {
      setFilterData(filterData.slice(0, 5));
      // checks if the requested array based on query is greater than 5 entries and culls response to 5
    } else {
      setFilterData(filterData);
      // if number of entries in array is less than or equal to 5 returns all entires
    }
  }, [query, data]);
  // useEffect upon page load and change to selected variable(s) executes defined code(function(s)) before comma

  return (
    <div className="container">
      <div className="ng-star-inserted">
        <h1 className="display-5 fw-bold">Saunter App</h1>
        <div
          className="px-4 py-5 my-5 card bg-dark text-white picturefill-background border-0 hero hero-cta   text-center"
          style={{
            backgroundImage: `url("https://images.unsplash.com/photo-1581351721010-8cf859cb14a4?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8&w=1000&q=80")`,
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "50% 50%",
          }}
        ></div>

        <div className="custom-bg" data-uia-nmhp="our-story-cards">
          <div
            className="our-story-card hero-card hero_fuji vlv"
            data-uia-nmhp="hero_fuji"
            data-uia="our-story-card"
          >
            <div className="our-story-card-background">
              <div
                className="concord-img-wrapper"
                data-uia="concord-img-wrapper"
                style={{ height: "690.594x" }}
              >
                <img
                  alt=""
                  className="bg-img"
                  src="https://images.unsplash.com/photo-1581351721010-8cf859cb14a4?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8&w=1000&q=80"
                ></img>
                <p className="aria-offscreen">a travel log service.</p>
                {
                  <div>
                    <input
                      placeholder="Search By City"
                      // choses what is displayed in search bar
                      onChange={(event) => setQuery(event.target.value)}
                      // on a change in search bar hands the searched value into the set query function
                    />
                    {/* <>
                      {
                        filterData.map((reviews) => (
                          //choses what fields to display and check
                          // key is the uniqe identifier
                          <div key={reviews.id}>
                            <p>{reviews.author}</p>
                            <p>{reviews.trip.total_cost}</p>
                            <p>{reviews.trip.location.name}</p>
                          </div>
                        ))
                      }
                    </> */}
                  </div>
                }
                <BlogCardList
                  setReviewId={props.setReviewId}
                  filterData={filterData}
                ></BlogCardList>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
