import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import { useState } from "react";
import MainPage from "./MainPage";
import Nav from "./Nav";
import LogIn from "./LogIn";
import SignUpForm from "./SignUp";
import { useToken } from "./authApi";
import BlogForm from "./BlogForm";
import "./App.css";
import TripCreateForm from "./TripCreate";
import TripListForm from "./TripList";
import TripDeleteForm from "./TripDelete";
import HomePage from "./Homepage";
import BlogList from "./BlogList";
import LocationCreateForm from "./LocationCreate";
import SearchBar from "./SearchBar";
import DeleteBlogForm from "./DeleteBlogForm";
import BlogDetailForm from "./BlogDetailForm";

function App(props) {
  const [token, login, logout, signup] = useToken();
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");

  const [reviewId, setReviewId] = useState();
  return (
    <BrowserRouter basename={basename}>
      <Nav />
      <div className="container-fluid">
        <Routes>
          <Route path="/" element={<MainPage setReviewId={setReviewId} />} />
          <Route
            path="/login"
            element={<LogIn token={token} login={login} logout={logout} />}
          />
          <Route
            path="/signup"
            element={<SignUpForm token={token} signup={signup} />}
          />
          <Route path="/createblog" element={<BlogForm token={token} />} />
          <Route path="/newtrip" element={<TripCreateForm token={token} />} />
          <Route path="/yourtrips" element={<TripListForm token={token} />} />
          <Route
            path="/tripdelete"
            element={<TripDeleteForm token={token} />}
          />
          <Route path="/homepage" element={<HomePage token={token} />} />
          <Route path="/reviews" element={<BlogList token={token} />} />
          <Route
            path="/newlocation"
            element={<LocationCreateForm token={token} />}
          />
          <Route path="/search" element={<SearchBar />} />
          <Route
            path="/blogdelete"
            element={<DeleteBlogForm token={token} />}
          />
          <Route
            path="/tripdelete"
            element={<TripDeleteForm token={token} />}
          />
          <Route
            path="/details"
            element={<BlogDetailForm reviewId={reviewId} />}
          />
        <Route path="*" element={<Navigate to="/" />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
